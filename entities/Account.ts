import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Account {
	@PrimaryGeneratedColumn()
	id: string;

	@Column("varchar", {
		length: 255,
	})
	username: string;

	@Column("text")
	display_name: string;

	@Column("timestamp")
	created_at: string;

	@Column("varchar", {
		length: 4096,
	})
	note: string;

	@Column("varchar")
	avatar: string;

	@Column("varchar")
	header: string;
}
