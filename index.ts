import { Serve } from "bun";
import { DataSource } from "typeorm";
import { Account } from "./entities/Account.js";
import "reflect-metadata";

const AppDataSource = new DataSource({
	type: "postgres",
	host: process.env.POSTGRES_HOST,
	port: Number(process.env.POSTGRES_PORT),
	username: process.env.POSTGRES_USERNAME,
	password: process.env.POSTGRES_PASSWORD,
	database: process.env.POSTGRES_DB,
	entities: [Account],
	synchronize: true,
	logging: false,
});

AppDataSource.initialize().catch(err => console.error(err));

const server: Serve = {
	port: 8080, // defaults to $PORT, then 3000
	async fetch(req) {
		const url = new URL(req.url);
		const accountRepository = AppDataSource.getRepository(Account);

		const paths = url.pathname.split("/");

		if (paths[1] === "api") {
			if (paths[2] === "accounts") {
				if (paths[3] === "create") {
					const account = new Account();

					account.avatar = "";
					account.created_at = new Date(Date.now())
						.toISOString()
						.replace("T", " ")
						.replace("Z", ""); // Generate Postgres-acceptable date format
					account.display_name = "Jesse";
					account.header = "";
					account.note = "Example note";
					account.username = "jesse";

					await AppDataSource.manager.save(account);
					return new Response(JSON.stringify(account), {
						headers: new Headers({
							"Content-Type": "application/ld+json",
						}),
					});
				} else {
					const id = paths[3];

					const account = await accountRepository.findOneBy({
						id: id,
					});

					if (!account) {
						return new Response(
							JSON.stringify({
								status: 404,
								error: "User not found",
							}),
							{
								status: 404,
							}
						);
					}

					return new Response(JSON.stringify(account), {
						headers: new Headers({
							"Content-Type": "application/ld+json",
						}),
						status: 200,
					});
				}
			}
		}

		return new Response("", {
			status: 404,
		});
	},
};

export default server;
