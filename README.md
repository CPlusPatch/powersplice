# Powersplice API

## Endpoints

### Accounts

`GET /api/accounts/[id]` Returns an Account object<br/>
⚠️ Returns a 404 when user is not found with an error message

`POST /api/accounts/create` Parameters:
 - `username`: string, username of new user (lowercase alphanumeric characters + underscore only)
 - `display_name`: string, display name of new user
 - `avatar`, `header` and `note` are not valid parameters and can be changed via the dedicated account editing endpoint

## Objects

### Account
```ts
id: number;
username: string;
display_name: string;
created_at: string;  // PostgreSQL timestamp
note: string;        // "About Me" profile section contents
avatar: string;      // Avatar URL
header: string;      // Banner URL
```